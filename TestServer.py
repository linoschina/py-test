# coding: utf-8
__author__ = 'linjinbin'

from flask import Flask, request, jsonify

app = Flask(__name__)

import logging
import json

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s',
                    datefmt='%a, %d %b %Y %H:%M:%S',
                    filename='./test.log',
                    filemode='w')
logging.getLogger('werkzeug').setLevel(logging.ERROR)
log = logging.getLogger("root")


@app.route('/ping')
def ping():
    return "pong", 200


# @app.route("/strategy", methods=['POST'])
# def strategy():
#     data = request.get_data()
#     log.info("data: " + data)
#     json_data = json.loads(data.decode("utf-8"))
#     datas = json_data.get("data")
#     newdatas = []
#     for da in datas:
#         da['ctr'] = da['weight']
#         newdatas.append(da)
#     log.info("newdatas: " + str(newdatas))
#     return jsonify(newdatas)


if __name__ == '__main__':
    host = "0.0.0.0"
    port = 19001
    log.info("start app at %s:%s", host, port)
    app.run(host=host, debug=False, port=port)
